import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 14.04.2017.
 */
public class contactsData extends baseClass {

    public contactsData(WebDriver driver){this.driver = driver;}

    public void setContactName(String strContactName){driver.findElement(contactName).sendKeys(strContactName);}

    public void setContactEmail(String strContactEmail){driver.findElement(contactEmail).sendKeys(strContactEmail);}

    public void setContactComment(String strContactComment){driver.findElement(contactComment).sendKeys(strContactComment);}

    public void setSendButton(){driver.findElement(sendButton).click();}

    public void enterContactsData(String strContactName, String strContactEmail, String strContactComment){

        this.setContactName(strContactName);
        this.setContactEmail(strContactEmail);
        this.setContactComment(strContactComment);
        this.setSendButton();

    }
}
