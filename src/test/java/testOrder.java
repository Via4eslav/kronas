import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by ivanov.v on 10.04.2017.
 */
public class testOrder {
    WebDriver driver;
    firstStep objFirstStep;
    secondStep objSecondStep;
    thirdStep objThirdStep;
    resultPage objResultPage;

    @BeforeTest

    public void setup(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://inkubator.ks.ua/html/kronas/order.html");
    }

    @Test

    public void testThisOrder(){
        objFirstStep = new firstStep(driver);
        objFirstStep.enterDataToFirst("Вячеслав", "Иванов", "ivanov.v.wezom@gmail.com","+380957731961", "789789", "789789");

        objSecondStep = new secondStep(driver);
        objSecondStep.enterDataToSecond("Димитрова 19", "Lorem ipsum, gospoda!");

        objThirdStep = new thirdStep(driver);
        objThirdStep.enterDataToThird();

        objResultPage = new resultPage(driver);
        //Assert.assertTrue(objResultPage.getTextOnPage().contains("Ваш заказ успешно обработан!"));
        Assert.assertEquals(objResultPage.getTextOnPage(), "Ваш заказ успешно обработан!");
    }

}
