import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 13.04.2017.
 */
public class baseClass {

    public WebDriver driver;

    //Step one locators

    By name = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div/div/div[3]/div/div[1]/div/input");
    By lastName = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div/div/div[3]/div/div[2]/div/input");
    By email = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div/div/div[3]/div/div[3]/div/input");
    By phone = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div/div/div[4]/div/div[1]/div/input");
    By password = By.xpath(".//*[@id='orderPassword']");
    By confirmPassword = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div/div/div[4]/div/div[3]/div/input");
    By termsOfUse = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div/div/div[10]/div/label/ins");
    By goToSecondStep = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[3]/div[2]/div[2]/div[2]/div[2]/div/div/div[11]/div");

    //Step two locators

    By delivery = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[4]/div[2]/div[2]/div[2]/div[1]/div/div/div[2]/div/label/ins");
    //By region = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[4]/div[2]/div[2]/div[2]/div[4]/div[1]/div/div[1]/div/span/span[1]/span");
    //By city = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[4]/div[2]/div[2]/div[2]/div[4]/div[1]/div/div[2]/div/span/span[1]/span");
    By addressDelivery = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[4]/div[2]/div[2]/div[2]/div[4]/div[1]/div/div[4]/div/label/ins");
    By address = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[4]/div[2]/div[2]/div[2]/div[4]/div[2]/div/div[1]/div/input");
    By commentField = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[4]/div[2]/div[2]/div[2]/div[4]/div[2]/div/div[2]/div/textarea");
    By goToThirdStep = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[4]/div[2]/div[2]/div[2]/div[4]/div[4]/div");

    //Step three locators

    By payout = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[5]/div[2]/div[2]/div[2]/div/div[1]/div/div[2]/div/label/ins");
    By deliveryTerms = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[5]/div[2]/div[2]/div[2]/div/div[2]/div/label/ins");
    By orderButton = By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[5]/div[2]/div[2]/div[2]/div/div[3]/div/div");

    //Result page locators

    By textOnPage = By.xpath("html/body/main/section/div/div[2]/div/p[1]");

    //Contact form locators

    By contactName = By.xpath("html/body/main/section/div[4]/div[2]/div[6]/div/div[2]/div[2]/div/div[1]/div/input");
    By contactEmail = By.xpath("html/body/main/section/div[4]/div[2]/div[6]/div/div[2]/div[2]/div/div[2]/div/input");
    By contactComment = By.xpath("html/body/main/section/div[4]/div[2]/div[6]/div/div[2]/div[2]/div/div[3]/div/textarea");
    By sendButton = By.xpath("html/body/main/section/div[4]/div[2]/div[6]/div/div[2]/div[2]/div/div[4]/div");

    //verify send contact form locators

    By confirmMessage = By.xpath("html/body/main/section/div[4]/div[2]/div[6]/div/div[2]/div[2]/div/div[4]/span/span/span/span[2]");

}
