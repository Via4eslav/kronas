import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by ivanov.v on 14.04.2017.
 */
public class testContactForm {
    WebDriver driver;
    contactsData objContactsData;
    verifySendContactForm objVerifyContactForm;

    @BeforeTest

    public void setBrowser(){

        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.get("http://inkubator.ks.ua/html/kronas/contacts.html");
    }

    @Test

    public void testingContactForm(){

        objContactsData = new contactsData(driver);
        objContactsData.enterContactsData("Иванов Вячеслав","ivanov.v.wezom@gmail.com","Lorem ipsum, gospoda!");

        objVerifyContactForm = new verifySendContactForm(driver);
        Assert.assertTrue(objVerifyContactForm.getConfirmText().contains("Комментарий успешно отправлен! Спасибо."));

    }
}
