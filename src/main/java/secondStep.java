import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by ivanov.v on 10.04.2017.
 */
public class secondStep extends baseClass {

    public secondStep(WebDriver driver) {this.driver = driver;}

    public void checkDelivery() {
        driver.findElement(delivery).click();
    }

    public void setRegion() {
        WebElement region = driver.findElement(By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[4]/div[2]/div[2]/div[2]/div[4]/div[1]/div/div[1]/div/span/span[1]/span"));
        region.click();
        region.sendKeys("Одесская область");
        region.sendKeys(Keys.ENTER);
    }

    public void setCity() {
        WebElement city = driver.findElement(By.xpath("html/body/main/section/div[1]/div[2]/div[1]/div[4]/div[2]/div[2]/div[2]/div[4]/div[1]/div/div[2]/div/span/span[1]/span"));
        city.click();
        city.sendKeys("Белозерка");
        city.sendKeys(Keys.ENTER);
    }

    public void setAddressDelivery() {driver.findElement(addressDelivery).click();}

    public void setAddress(String strAddress) {driver.findElement(address).sendKeys(strAddress);}

    public void setComment(String strComment) {driver.findElement(commentField).sendKeys(strComment);}

    public void clickThirdStep() {driver.findElement(goToThirdStep).click();}

    public void enterDataToSecond(String strAddress, String strComment){

        this.checkDelivery();
        this.setRegion();
        this.setCity();
        this.setAddressDelivery();
        this.setAddress(strAddress);
        this.setComment(strComment);
        this.clickThirdStep();
    }


}