import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 10.04.2017.
 */
public class resultPage extends baseClass {

    public resultPage(WebDriver driver) {this.driver = driver;}

    public String getTextOnPage() {return driver.findElement(textOnPage).getText();}
}
