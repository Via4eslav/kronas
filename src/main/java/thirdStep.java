import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 10.04.2017.
 */
public class thirdStep extends baseClass {

        public thirdStep(WebDriver driver){this.driver = driver;}

        public void setPayout(){driver.findElement(payout).click();}

        public void setDeliveryTerms() {driver.findElement(deliveryTerms).click();}

        public void setOrderButton() {driver.findElement(orderButton).click();}

        public void enterDataToThird(){

            this.setPayout();
            this.setDeliveryTerms();
            this.setOrderButton();
        }

}
