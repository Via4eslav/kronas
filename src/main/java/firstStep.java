
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

    public class firstStep extends baseClass {

        public firstStep(WebDriver driver) {
            this.driver = driver;
        }

        public void setName(String strName) {
            this.driver.findElement(this.name).sendKeys(strName);
        }

        public void setLastName(String strLastName) {
            this.driver.findElement(this.lastName).sendKeys(strLastName);
        }

        public void setEmail(String strEmail) {
            this.driver.findElement(this.email).sendKeys(strEmail);
        }

        public void setPhone(String strPhone) {
            this.driver.findElement(this.phone).sendKeys(strPhone);
        }

        public void setPassword(String strPassword) {
            this.driver.findElement(this.password).sendKeys(strPassword);
        }

        public void setCnfrmPsswrd(String strConfirmPassword) {
            this.driver.findElement(this.confirmPassword).sendKeys(strConfirmPassword);
        }

        public void setTermsOfUse() {
            this.driver.findElement(this.termsOfUse).click();
        }

        public void setSecondStep() {
            this.driver.findElement(this.goToSecondStep).click();
        }

        public void enterDataToFirst(String strName, String strLastName, String strEmail, String strPhone, String strPassword, String strConfirmPassword) {
            this.setName(strName);
            this.setLastName(strLastName);
            this.setEmail(strEmail);
            this.setPhone(strPhone);
            this.setPassword(strPassword);
            this.setCnfrmPsswrd(strConfirmPassword);
            this.setTermsOfUse();
            this.setSecondStep();
        }
    }

