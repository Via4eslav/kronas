import org.openqa.selenium.WebDriver;

/**
 * Created by ivanov.v on 14.04.2017.
 */
public class verifySendContactForm extends baseClass{

    public verifySendContactForm(WebDriver driver){this.driver = driver;}

    public String getConfirmText(){return driver.findElement(confirmMessage).getText();}
}
